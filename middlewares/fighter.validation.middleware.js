const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
  const {
    name = fighter.name,
    health = fighter.health,
    power = fighter.power,
    defense = fighter.defense,
  } = req.body;
  const nameValid = name.length >= 1 && !!name.match(/^[a-zA-z0-9]+$/);
  const powerValid = power < 100;
  const defenseValid = defense <= 10 && defense >= fighter.defense;

  if (!nameValid || !powerValid || !defenseValid) {
    return res.status(400).json({
      error: true,
      message: 'Fighter entity to create is not valid',
    });
  }

  res.fighter = {
    name,
    health: Number(health),
    power: Number(power),
    defense: Number(defense),
  };

  next();
};

const updateFighterValid = (req, res, next) => {
  const { name, health, power, defense } = req.body;
  let updateHealth, updatePower, updateDefense;
  let nameValid = true,
    healthValid = true,
    powerValid = true,
    defenseValid = true;

  if (name) {
    nameValid = name.length >= 1 && !!name.match(/^[a-zA-z0-9]+$/);
  }

  if (health) {
    healthValid = health <= 100 && health > 0;
    updateHealth = Number(health);
  }

  if (power) {
    powerValid = power < 100;
    updatePower = Number(power);
  }

  if (defense) {
    defenseValid = defense <= 10 && defense >= fighter.defense;
    updateDefense = Number(defense);
  }

  if (!nameValid || !healthValid || !powerValid || !defenseValid) {
    return res.status(400).json({
      error: true,
      message: 'Fighter entity to update is not valid',
    });
  }

  res.updateFighter = {
    name,
    health: updateHealth,
    power: updatePower,
    defense: updateDefense,
  };

  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
