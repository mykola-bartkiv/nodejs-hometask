const responseMiddleware = (req, res, next) => {
  let jsonObj,
    statusCode = 200;

  if (res.err) {
    statusCode = res.statusCode || 400;
    jsonObj = {
      error: true,
      message: res.err.message,
    };
  } else if (res.data) {
    jsonObj = res.data;
  } else {
    jsonObj = {
      success: true,
      message: res.message,
    };
  }

  res.status(statusCode).json(jsonObj);

  next();
};

exports.responseMiddleware = responseMiddleware;
