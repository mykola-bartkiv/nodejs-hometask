const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
  const { email = '', phoneNumber = '', password = '' } = req.body;

  const phoneValid =
    phoneNumber.length === 13 &&
    phoneNumber.startsWith('+380') &&
    !!phoneNumber.slice(1).match(/^[0-9]+$/);
  const emailValid =
    email.endsWith('@gmail.com') &&
    !!email.slice(0, email.indexOf('@gmail.com')).match(/^[a-zA-z0-9]+$/);
  const passwordValid = password.length >= 3;

  if (!phoneValid || !emailValid || !passwordValid) {
    return res.status(400).json({
      error: true,
      message: 'User entity to create is not valid',
    });
  }

  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
