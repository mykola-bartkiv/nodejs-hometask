const { UserRepository } = require('../repositories/userRepository');

class UserService {
  removeProperty(data, exclusion) {
    data = Array.isArray(data) ? data : [data];
    const properties = exclusion.split(' ');
    const users = JSON.parse(JSON.stringify(data));

    return users.map((user) => {
      for (let property of properties) {
        if (!user.hasOwnProperty(property)) {
          continue;
        }

        delete user[property];
      }

      return user;
    });
  }

  update(id, dataToUpdate) {
    return UserRepository.update(id, dataToUpdate);
  }

  getAll() {
    return UserRepository.getAll();
  }

  create(data) {
    return UserRepository.create(data);
  }

  delete(id) {
    return UserRepository.delete(id);
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
