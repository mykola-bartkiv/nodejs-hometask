const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  removeProperty(data, exclusion) {
    data = Array.isArray(data) ? data : [data];
    const properties = exclusion.split(' ');
    const fighters = JSON.parse(JSON.stringify(data));

    return fighters.map((fighter) => {
      for (let property of properties) {
        if (!fighter.hasOwnProperty(property)) {
          continue;
        }

        delete fighter[property];
      }

      return fighter;
    });
  }

  update(id, dataToUpdate) {
    return FighterRepository.update(id, dataToUpdate);
  }

  getAll() {
    return FighterRepository.getAll();
  }

  create(data) {
    return FighterRepository.create(data);
  }

  delete(id) {
    return FighterRepository.delete(id);
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
