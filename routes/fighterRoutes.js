const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    try {
      if (FighterService.getAll().length === 0) {
        throw Error('Fighters not found');
      }

      const fighters = FighterService.getAll();

      res.data = FighterService.removeProperty(
        fighters,
        'id createdAt updatedAt'
      );
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;
      if (!FighterService.search({ id })) {
        res.statusCode = 404;
        throw Error('Fighter not found');
      }

      const fighter = FighterService.search({ id });

      res.data = FighterService.removeProperty(
        fighter,
        'id createdAt updatedAt'
      );
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

// router.put(
//   '/:id',
//   updateFighterValid,
//   (req, res, next) => {
//     try {
//       const { id } = req.params;
//       let selectedFighter = FighterService.search({ id });
//       if (!selectedFighter) {
//         res.statusCode = 404;
//         throw Error('Fighter not found');
//       }

//       const updateFighter = { ...res.updateFighter };

//       for (var key in updateFighter) {
//         if (updateFighter[key] === '') {
//           delete updateFighter[key];
//         }
//       }

//       const dataToUpdate = Object.assign(selectedFighter, updateFighter);
//       //const updateFighter = FighterService.update(id, res.updateFighter);

//       res.data = dataToUpdate;
//     } catch (err) {
//       res.err = err;
//     } finally {
//       next();
//     }
//   },
//   responseMiddleware
// );

router.post(
  '/',
  createFighterValid,
  (req, res, next) => {
    try {
      const { name } = req.body;

      if (FighterService.search({ name })) {
        throw Error('Fighter already exists');
      }

      const fighter = FighterService.create(res.fighter);

      res.fighter = fighter;
      res.message = 'Fighter created';
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;

      if (!FighterService.search({ id })) {
        res.statusCode = 404;
        throw Error('Fighter not found');
      }

      FighterService.delete(id);
      res.message = 'Fighter deleted';
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
