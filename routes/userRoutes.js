const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    try {
      if (UserService.getAll().length === 0) {
        res.statusCode = 404;
        throw Error('Users not found');
      }

      const users = UserService.getAll();

      res.data = UserService.removeProperty(
        users,
        'id createdAt updatedAt password phoneNumber lastName'
      );
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;
      if (!UserService.search({ id })) {
        res.statusCode = 404;
        throw Error('User not found');
      }

      const user = UserService.search({ id });

      res.data = UserService.removeProperty(
        user,
        'id createdAt updatedAt password phoneNumber lastName'
      );
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  createUserValid,
  (req, res, next) => {
    try {
      const { email } = req.body;

      if (UserService.search({ email })) {
        throw Error('User already exists');
      }

      const newUser = { ...req.body };

      for (var key in newUser) {
        if (newUser[key] === '') {
          delete newUser[key];
        }
      }

      const data = UserService.create(newUser);

      res.message = 'User created';
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    try {
      const { id } = req.params;

      if (!UserService.search({ id })) {
        res.statusCode = 404;
        throw Error('User not found');
      }

      UserService.delete(id);
      res.message = 'User deleted';
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
