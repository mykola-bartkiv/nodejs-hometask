const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/login',
  (req, res, next) => {
    try {
      const { email, password } = req.body;
      const data = AuthService.login({ email });

      if (data.password !== password) {
        throw Error('Wrong password');
      }

      res.data = data;
      res.message = 'User logged in';
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
